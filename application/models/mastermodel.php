<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MasterModel extends CI_Model {

    public function querySlider() {
        $this->db->where('enable_status', 'show');
        $this->db->order_by('sort_priority');
        return $this->db->get('tbl_slider');
    }

    public function getTopBanner($id = 0) {
        $this->db->where('top_banner_lists_id', $id);
        $query = $this->db->get('tbl_top_banner_lists');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function getFooterBanner($id = 0) {
        $this->db->where('bottom_banner_lists_id', $id);
        $query = $this->db->get('tbl_bottom_banner_lists');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function getStaticContent($table, $field) {
        $this->db->select($field);
        $this->db->where($table . '_id', 1);
        $query = $this->db->get('tbl_' . $table);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->$field;
        } else {
            return '';
        }
    }

    public function getList($table, $per_page = 0, $start_at = 0) {
        $this->db->where('enable_status', 'show');
        $this->db->order_by('sort_priority');
        if ($per_page > 0) {
            $this->db->limit($per_page,$start_at);
        }
        return $this->db->get('tbl_' . $table);
    }

    public function getNumrow($table) {
        $this->db->where('enable_status', 'show');
        $query = $this->db->get('tbl_' . $table);
        return $query->num_rows();
    }

    public function getDigestDetail($id) {
        $this->db->where('golf_digest_lists_id', $id);
        $query = $this->db->get('tbl_golf_digest_lists');
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row;
        } else {
            return array();
        }
    }

}
