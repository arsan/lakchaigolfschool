<tr>
    <td align="center"><table width="1000" cellpadding="0" cellspacing="0">
            <tr>
                <td height="20">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top" bgcolor="686868" class="tableOrangeBorder"><table width="1000" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td><table width="240" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img src="<?php echo base_url('assets/images/header_digest.jpg'); ?>" width="240" height="65" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="970" cellspacing="0" cellpadding="0">
                                    <?php
                                    $num_rows = $this->MasterModel->getNumrow('golf_digest_lists');
                                    $start_at = (($per_page * $page) - $per_page);
                                    $query = $this->MasterModel->getList('golf_digest_lists', $per_page, $start_at);

                                    if ($num_rows <= $per_page) {
                                        $num_pages = 1;
                                    } else if (($num_rows % $per_page) == 0) {
                                        $num_pages = ($num_rows / $per_page);
                                    } else {
                                        $num_pages = ($num_rows / $per_page) + 1;
                                        $num_pages = (int) $num_pages;
                                    }
                                    $prev_page = $page - 1;
                                    $next_page = $page + 1;
                                    foreach ($query->result() as $row) {
                                        ?>
                                        <tr>
                                            <td><table width="970" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="225"><a href="<?php echo base_url('digest/detail/' . $row->golf_digest_lists_id); ?>"><img src="<?php echo base_url('../' . $row->thumbnail); ?>" width="225" height="125" /></a></td>
                                                        <td width="20">&nbsp;</td>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="30" class="circularBoldBlack"><a href="<?php echo base_url('digest/detail/' . $row->golf_digest_lists_id); ?>"><?php echo $row->title; ?></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="circular"><a href="<?php echo base_url('digest/detail/' . $row->golf_digest_lists_id); ?>"><?php echo $row->sub_title; ?></a></td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20"><img src="<?php echo base_url('assets/images/line_02.gif'); ?>" width="970" height="1" /></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                                <p class="pager">
                                    <!--Total <?php echo $num_rows; ?> Record : <?php echo $num_pages; ?> Page :-->
                                    <?php
                                    if ($prev_page) {
                                        echo ' <a href="' . base_url('digest/index/' . $prev_page) . '"><< Back</a> ';
                                    }

                                    for ($i = 1; $i <= $num_pages; $i++) {
                                        if ($i != $page) {
                                            echo '[ <a href="' . base_url('digest/index/' . $i) . '">' . $i . '</a> ]';
                                        } else {
                                            echo "<b> $i </b>";
                                        }
                                    }
                                    if ($page != $num_pages) {
                                        echo ' <a href="' . base_url('digest/index/' . $next_page) . '">Next >></a> ';
                                    }
                                    ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
        </table>
    </td>
</tr>