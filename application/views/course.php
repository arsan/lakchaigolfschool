<tr>
    <td align="center">
        <table width="1000" cellpadding="0" cellspacing="0">
            <tr>
                <td height="20">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top" bgcolor="686868" class="tableOrangeBorder"><table width="1000" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="240" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img src="<?php echo base_url('assets/images/header_course.jpg'); ?>" width="240" height="65" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="920" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <?php echo $this->MasterModel->getStaticContent('course', 'content'); ?>
                                        </td>
                                    </tr>
                                </table>
                                <table width="920" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php
                                    $query = $this->MasterModel->getList('course_lists');
                                    foreach ($query->result() as $row) {
                                        ?>
                                        <tr>
                                            <td>
                                                <h2><?php echo $row->title; ?></h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <?php echo $row->detail; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="40">&nbsp;</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
        </table>

    </td>
</tr>