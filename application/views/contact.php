<tr>
    <td align="center">
        <table width="1000" cellpadding="0" cellspacing="0">
            <tr>
                <td height="20">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top" bgcolor="686868" class="tableOrangeBorder">
                    <table width="1000" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="240" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img src="<?php echo base_url('assets/images/header_contact.jpg'); ?>" width="240" height="65" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="800" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="circular"><span class="circularBold">
                                            <?php echo $this->MasterModel->getStaticContent('contact', 'address'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="300" bgcolor="#FFFFFF">
                                            <?php echo $this->MasterModel->getStaticContent('contact', 'google_map'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="circular">
                                            <?php echo $this->MasterModel->getStaticContent('contact', 'contact_info'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TextWhite01">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="TextWhite01">
                                            <table width="800" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="20" bgcolor="444444">&nbsp;</td>
                                                    <td height="60" bgcolor="444444"><span class="TextWhite03">CONTACT US</span><span class="TextWhiteContactHeader"><br />
                                                            <span class="TextWhite01">(*&nbsp;Important info, Please fill in correctly) </span></span></td>
                                                    <td bgcolor="444444">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td height="220">
                                                        <form method="post" action="<?php echo base_url('contact/sendmail');?>">
                                                            <table width="750" cellpadding="0" cellspacing="5">
                                                                <tr>
                                                                    <td width="350" class="TextWhite01">Name : * </td>
                                                                    <td class="TextWhite01">Message : * </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input name="name" type="text" class="TextfieldBox01" width="280" height="18" /></td>
                                                                    <td rowspan="5" valign="top"><textarea name="msg" class="TextfieldBox02" id="msg" width="280" height="18"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="TextWhite01">Phone : </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input name="phone" type="text" class="TextfieldBox01" id="phone" width="280" height="18" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="TextWhite01">Email : * </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input name="email" type="text" class="TextfieldBox01" id="email" width="280" height="18" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td valign="top"><input name="submit" type="submit" value="Send" /></td>
                                                                </tr>
                                                            </table>
                                                        </form>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
        </table>

    </td>
</tr>