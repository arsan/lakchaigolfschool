<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link rel="shortcut icon" href="<?php echo base_url('assets/images/icon.ico'); ?>" type="image/x-icon" /> 
        <link rel="icon" href="<?php echo base_url('assets/images/icon.ico'); ?>" type="image/x-icon" />
        <link href="<?php echo base_url('assets/CSS/main.css'); ?>" rel="stylesheet" type="text/css">
            <link href="<?php echo base_url('assets/CSS/digest.css'); ?>" rel="stylesheet" type="text/css">
                <!--script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script-->
	<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'></script>
                <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>

                <script src="<?php echo base_url('assets/js/tinyfader.js'); ?>"></script>
                <title>LAKCHAI GOLF SCHOOL</title>
                <script type="text/JavaScript">
                    <!--
                    function MM_swapImgRestore() { //v3.0
                    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
                    }

                    function MM_swapImage() { //v3.0
                    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                    if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
                    }
                    //-->
                </script>
                <script type="text/javascript">
                    $(function() {
                        <?php if($pagename != 'home'){?>
                        $.scrollTo( '#main-navigate');
                        <?php }?>
                    });
                </script>
                </head>

                <body bgcolor="#333333" background="" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onload="MM_preloadImages('<?php echo base_url('assets/images/menu/Menu_over_02.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_01.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_03.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_04.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_05.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_06.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_07.gif'); ?>', '<?php echo base_url('assets/images/menu/Menu_over_08.gif'); ?>')">
                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" valign="top"><table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="320" align="center" background="<?php echo base_url('assets/images/BG01.jpg'); ?>" bgcolor="#242424"><table width="1200" height="320" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <div style="position:relative; float:none; height: 0px; width: 0px; top: 0px; left: 0px; z-index:150">
                                                            <div style="position:relative; float:none; height: 320px; width: 140px; top: 0px; left: 0px; z-index:160">
                                                                <table width="140" height="320" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td bgcolor="#FFFFFF"><table width="140" height="320" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td height="140" align="center" valign="middle"><img src="<?php echo base_url('assets/images/Logo_01.png'); ?>" width="140" height="140" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="bottom">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="40" align="center" valign="middle">
                                                                                        <table width="130" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td align="center"><a href="<?php echo $this->MasterModel->getStaticContent('home', 'facebook'); ?>" target="_blank"><img src="<?php echo base_url('assets/images/logo_Facebook.png'); ?>" width="30" height="31" border="0" /></a></td>
                                                                                                <td align="center"><a href="<?php echo $this->MasterModel->getStaticContent('home', 'twitter'); ?>"><img src="<?php echo base_url('assets/images/logo_Twitter.png'); ?>" width="30" height="31" border="0" /></a></td>
                                                                                                <td align="center"><a href="<?php echo $this->MasterModel->getStaticContent('home', 'youtube'); ?>"><img src="<?php echo base_url('assets/images/logo_Youtube.png'); ?>" width="30" height="30" border="0" /></a></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div id="wrapper">
                                                            <div id="slideshow">
                                                                <ul id="slides" name="slides">
                                                                    <?php
                                                                    $query = $this->MasterModel->querySlider();
                                                                    foreach ($query->result() as $row) {
                                                                        ?>
                                                                        <li><img src="<?php echo base_url('../' . $row->image); ?>" width="1200" height="320" alt="" /></li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </ul>
                                                            </div>
                                                        </div>		
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>			
                                                    </td>
                                                </tr>
                                            </table>		
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>		
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>		
                                        </td>
                                    </tr>
                                    <tr id="main-navigate">
                                        <td height="40" align="center" background="<?php echo base_url('assets/images/menu_BG.gif'); ?>">
                                            <table height="40" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center"><a href="<?php echo base_url('home'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('home', '', '<?php echo base_url('assets/images/menu/Menu_over_01.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_01.gif'); ?>" alt="Home" name="Home" width="67" height="40" border="0" id="home" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('about'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('about', '', '<?php echo base_url('assets/images/menu/Menu_over_02.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_02.gif'); ?>" alt="about" name="About" width="121" height="40" border="0" id="about" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('course'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Course', '', '<?php echo base_url('assets/images/menu/Menu_over_03.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_03.gif'); ?>" alt="Course" name="Course" width="122" height="40" border="0" id="Course" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('promotion'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Promotion', '', '<?php echo base_url('assets/images/menu/Menu_over_04.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_04.gif'); ?>" alt="Promotion" name="Promotion" width="104" height="40" border="0" id="Promotion" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('technology'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Technology', '', '<?php echo base_url('assets/images/menu/Menu_over_05.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_05.gif'); ?>" alt="Technology" name="Technology" width="107" height="40" border="0" id="Technology" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('study'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Study', '', '<?php echo base_url('assets/images/menu/Menu_over_06.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_06.gif'); ?>" alt="Study" name="Study" width="158" height="40" border="0" id="Study" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('blog'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Blog', '', '<?php echo base_url('assets/images/menu/Menu_over_07.gif'); ?>', 1)" target="_blank"><img src="<?php echo base_url('assets/images/menu/Menu_07.gif'); ?>" alt="Blog" name="Blog" width="87" height="40" border="0" id="Blog" /></a></td>
                                                    <td width="3" align="center"><img src="<?php echo base_url('assets/images/line_01.gif'); ?>" width="1" height="36" /></td>
                                                    <td align="center"><a href="<?php echo base_url('contact'); ?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Contact', '', '<?php echo base_url('assets/images/menu/Menu_over_08.gif'); ?>', 1)"><img src="<?php echo base_url('assets/images/menu/Menu_08.gif'); ?>" alt="Contact" name="Contact" width="111" height="40" border="0" id="Contact" /></a></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="30" align="center" class="blank">&nbsp;
                                            <table width="1200" height="10" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <ul class="pagination" id="pagination" name="pagination">
                                                                <?php
                                                                $count = 0;
                                                                $query = $this->MasterModel->querySlider();
                                                                foreach ($query->result() as $row) {
                                                                    ?>
                                                                    <li onclick="slideshow.pos(<?php echo $count++; ?>)"></li>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <script type="text/javascript">
                                                            var slideshow = new TINY.fader.fade('slideshow', {
                                                                id: 'slides',
                                                                auto: 4,
                                                                resume: true,
                                                                navid: 'pagination',
                                                                activeclass: 'current',
                                                                visible: true,
                                                                position: 0,
                                                            });
                                                        </script>              
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="210" align="center">
                                            <table width="1000" cellpadding="0" cellspacing="0" class="banner">
                                                <tr>
                                                    <?php $row = $this->MasterModel->getTopBanner(1); ?>
                                                    <td width="310" height="170">
                                                        <a href="<?php echo $row->link; ?>" target="_self" title="<?php echo $row->title; ?>">
                                                            <img src="<?php echo base_url('../' . $row->thumbnail); ?>" width="300" height="160" border="0" />
                                                        </a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <?php $row = $this->MasterModel->getTopBanner(2); ?>
                                                    <td width="310" height="170">
                                                        <a href="<?php echo $row->link; ?>" target="_self" title="<?php echo $row->title; ?>">
                                                            <img src="<?php echo base_url('../' . $row->thumbnail); ?>" width="300" height="160" border="0" />
                                                        </a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <?php $row = $this->MasterModel->getTopBanner(3); ?>
                                                    <td width="310" height="170">
                                                        <a href="<?php echo $row->link; ?>" target="_self" title="<?php echo $row->title; ?>">
                                                            <img src="<?php echo base_url('../' . $row->thumbnail); ?>" width="300" height="160" border="0" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php echo $content; ?>
                                    <?php
                                    $query = $this->MasterModel->getList('footer');
                                    foreach ($query->result() as $row) {
                                        ?>
                                        <tr>
                                            <td align="center" bgcolor="<?php echo str_replace('#', '', $row->bg_color); ?>">
                                                <table width="1000" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <a href="<?php echo $row->link; ?>" target="_blank" title="<?php echo $row->title; ?>"><img src="<?php echo base_url('../' . $row->image); ?>"/></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td height="30" align="center" class="TextFooter">2014 © All Rights Reserved Lakchai Golf School </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </body>
                </html>