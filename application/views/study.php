<tr>
    <td align="center">
        <table width="1000" cellpadding="0" cellspacing="0">
            <tr>
                <td height="20">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="1000" cellpadding="0" cellspacing="0" bgcolor="#EB8514">
                        <tr>
                            <td width="385" align="left" valign="middle">
                                <table width="385" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="240" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="<?php echo base_url('assets/images/header_lesson.jpg'); ?>" width="240" height="65" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><img src="<?php echo base_url('../'.$this->MasterModel->getStaticContent('teaching', 'image')); ?>" width="294" height="400" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center">&nbsp;</td>
                                    </tr>
                                </table>

                            </td>
                            <td align="center" valign="middle" class="TextWhite01">
                                <?php echo $this->MasterModel->getStaticContent('teaching', 'content'); ?>              
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
    <td align="center" bgcolor="#666666"><table width="1000" cellspacing="0" cellpadding="0">
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
            <tr>
                <td><img src="<?php echo base_url('assets/images/Clip_header.png'); ?>" width="949" height="32" /></td>
            </tr>
            <tr>
                <td align="center">&nbsp;</td>
            </tr>
            <tr>
                <td align="left">
                    <table width="880" cellpadding="0" cellspacing="0">
                        <tr>
                            <?php
                            $query = $this->MasterModel->getList('youtube_lists');
                            foreach ($query->result() as $row) {
                                ?>
                                <td width="382">
                                    <table width="380" height="233" cellpadding="0" cellspacing="0" class="banner">
                                        <tr>
                                            <td>
                                                <?php echo $row->embed_code;?><br/><h3 style="color: #fff;margin-top: 0.6em;"><center><?php echo $row->title;?></center></h3>
                                            </td>
                                        </tr>
                                    </table>                  
                                </td>
                                <td>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
        </table>
    </td>
</tr>