<tr>
    <td align="center">
        <table width="1000" cellpadding="0" cellspacing="0">
            <tr>
                <td height="20">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="1000" height="570" cellpadding="0" cellspacing="0" bgcolor="#EB8514">
                        
                        <tr>
                            <td width="385" align="center"><img src="<?php echo base_url('../'.$this->MasterModel->getStaticContent('home', 'image')); ?>" width="345" height="550" /></td>
                            <td align="center" valign="middle" class="TextWhite01">
                                <?php echo $this->MasterModel->getStaticContent('home', 'content'); ?>    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td height="230" align="center">
        <table width="1000" cellpadding="0" cellspacing="0" class="banner">
            <tr>
                <td width="310" height="170">
                    <?php
                    $row = $this->MasterModel->getFooterBanner(1);
                    ?>
                    <a href="<?php echo $row->link; ?>" title="<?php echo $row->title; ?>">
                        <img src="<?php echo base_url('../'.$row->thumbnail); ?>" width="300" height="160" />
                    </a>
                </td>
                <td>&nbsp;</td>
                <td width="310" height="170">
                    <?php
                    $row = $this->MasterModel->getFooterBanner(2);
                    ?>
                    <a href="<?php echo $row->link; ?>" title="<?php echo $row->title; ?>">
                        <img src="<?php echo base_url('../'.$row->thumbnail); ?>" width="300" height="160" />
                    </a>
                </td>
                <td>&nbsp;</td>
                <td width="310" height="170">
                    <?php
                    $row = $this->MasterModel->getFooterBanner(3);
                    ?>
                    <a href="<?php echo $row->link; ?>" title="<?php echo $row->title; ?>">
                        <img src="<?php echo base_url('../'.$row->thumbnail); ?>" width="300" height="160" />
                    </a>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td align="center" bgcolor="#EB8514" class="blank" height="5">&nbsp;</td>
</tr>
<tr>
    <td align="center" bgcolor="#666666">
        <table width="1000" cellspacing="0" cellpadding="0">
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
            <tr>
                <td><img src="<?php echo base_url('assets/images/Blog_header.png'); ?>" width="949" height="32" /></td>
            </tr>
            <tr>
                <td align="center"><table width="900" cellspacing="0" cellpadding="0">
                        <?php
                        $query = $this->MasterModel->getList('blog_lists');
                        foreach ($query->result() as $row) {
                            ?>
                            <tr>
                                <td height="20">&nbsp;</td>
                                <td width="30">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="172" height="94"><img src="<?php echo base_url('../'.$row->thumbnail); ?>" width="172" height="94" border="0" /></td>
                                <td>&nbsp;</td>
                                <td>
                                    <table width="600" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="circularBold"><a href="<?php echo $row->link; ?>" target="_blank"><?php echo $row->title; ?></a> </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="circular"><?php echo $row->sub_title; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
        </table>
    </td>
</tr>