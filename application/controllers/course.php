<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Course extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('course', $dataContent, true);
        $data['pagename'] = 'course';
        $this->load->view('masterpage', $data);
    }

}
