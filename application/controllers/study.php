<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Study extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('study', $dataContent, true);
        $data['pagename'] = 'studio';
        $this->load->view('masterpage', $data);
    }

}
