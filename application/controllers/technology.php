<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Technology extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('technology', $dataContent, true);
        $data['pagename'] = 'technology';
        $this->load->view('masterpage', $data);
    }

}
