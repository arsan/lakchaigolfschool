<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Digest extends CI_Controller {

    public function index($page = 1) {
        $data = array();
        $dataContent = array();
        $dataContent['per_page'] = 12;
        $dataContent['page'] = $page;
        $data['content'] = $this->load->view('digest', $dataContent, true);
        $data['pagename'] = 'digest';
        $this->load->view('masterpage', $data);
    }

    public function detail($id = 0) {
        $data = array();
        $dataContent = array();
        $dataContent['row'] = $this->MasterModel->getDigestDetail($id);
        $data['content'] = $this->load->view('digest_detail', $dataContent, true);
        $data['pagename'] = 'digest';
        $this->load->view('masterpage', $data);
    }

}
