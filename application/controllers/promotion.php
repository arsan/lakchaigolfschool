<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promotion extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('promotion', $dataContent, true);
        $data['pagename'] = 'promotion';
        $this->load->view('masterpage', $data);
    }

}
