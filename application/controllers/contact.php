<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('contact', $dataContent, true);
        $data['pagename'] = 'contact';
        $this->load->view('masterpage', $data);
    }

    public function sendmail() {
        $this->load->model('SendMailModel');
        if (!$this->SpamModel->check_spam($_POST['name']) && !$this->SpamModel->check_spam($_POST['phone']) && !$this->SpamModel->check_spam($_POST['email']) && !$this->SpamModel->check_spam($_POST['msg'])) {
            $this->db->set('name', $_POST['name']);
            $this->db->set('phone', $_POST['phone']);
            $this->db->set('email', $_POST['email']);
            $this->db->set('message', $_POST['msg']);
            $this->db->set('parent_id', 0);
            $this->db->set('enable_status', 'show');
            $this->db->set('sort_priority', '1');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->set('mother_shop_id', 1);
            $this->db->insert('tbl_contact_us_list');

            $mail_subject = 'ข้อความจาก :' . $_POST['contact_name'];
            $mail_message = "คุณได้รับข้อความจาก Contact Form ในเว็บ Lakchai Golf School ดังนี้<br><br>";
            $mail_message .= "<b>คุณได้รับข้อความจาก</b>" . $_POST['name'] . "<br><b>Email</b> : " . $_POST['email'] . "<br><b>เบอร์โทรศัพท์</b> : " . $_POST['phone'] . "<br><br><b>เนื้อหา</b> : " . $_POST['msg'];
            $this->SendMailModel->smtpmail('lakchaigolf@gmail.com', $mail_subject, $mail_message);
            ob_start();
            redirect('contact');
        } else {
            redirect('contact');
        }
    }

}
